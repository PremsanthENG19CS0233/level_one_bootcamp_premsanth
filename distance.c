#include <stdio.h>
#include<math.h>

float input()
{
    float temp;
    scanf("%f",&temp);
    return temp;
}

float calc_dist(float a,float b)
{
    float result;
  
    result = sqrt(a*a + b*b);
    return result;
}   

void show_output(float x, float y, float output)
{
    printf("The distance between the points %f and %f is %2f",x,y,output);
}

int main()
{
    float i1,i2,j1,j2,a,b,dist;
    printf("First point:\n");
    printf("Enter The Value for a:\n");
    i1 = input();
    printf("Enter the Value For b:\n");
    j1 = input();
    
    printf("Second point:\n");
    printf("Enter the Value For a:\n");
    i2 = input();
    printf("Enter the Value For b:\n");
    j2 = input();
    
    a = (i2 - i1);
    b = (j2 - j1);
    
    dist = calc_dist(a, b);
    show_output(a,b,dist);

    return 0;
}
